package com.fitkit.doctor.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fitkit.doctor.R;
import com.fitkit.doctor.dashboard.OnclickListener;

import java.util.ArrayList;

public class VedioCallApointmentAdapter extends RecyclerView.Adapter<VedioCallApointmentAdapter.VedioCallAppointmentViewHolder>{

    private Context mContext;
    private ArrayList<String> mArrayList;
    private OnclickListener mOnclickListener;

    public VedioCallApointmentAdapter(Context context , ArrayList<String> arrayList, OnclickListener onclickListener) {
        mContext = context;
        mArrayList = arrayList;
        mOnclickListener = onclickListener;
    }

    @NonNull
    @Override
    public VedioCallAppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View listItem = inflater.inflate(R.layout.video_consultent_listview,parent, false);
        VedioCallAppointmentViewHolder viewHolder = new VedioCallAppointmentViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VedioCallAppointmentViewHolder holder, int position) {

        holder.mName.setText(mArrayList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnclickListener.OnAppointmentClick();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public static class VedioCallAppointmentViewHolder extends RecyclerView.ViewHolder{

        protected TextView mName;
        public VedioCallAppointmentViewHolder(@NonNull View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.doctor_name_txt);
        }
    }
}
