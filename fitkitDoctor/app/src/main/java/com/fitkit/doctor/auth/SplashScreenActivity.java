package com.fitkit.doctor.auth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.fitkit.doctor.R;
import com.fitkit.doctor.databinding.ActivitySplashScreenBinding;

public class SplashScreenActivity extends AppCompatActivity {

private ActivitySplashScreenBinding mBinding;
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash_screen);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_from_left);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}