package com.fitkit.doctor.auth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.fitkit.doctor.R;
import com.fitkit.doctor.dashboard.DashBoardActivity;
import com.fitkit.doctor.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);

//        ClickableSpan clickableSpan = new ClickableSpan() {
//            @Override
//            public void onClick(View widget) {
//                Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_from_left);
//            }
//        };

        SpannableStringBuilder spannable = new SpannableStringBuilder("Don’t have an account?  Signup");
        spannable.setSpan(
                new ForegroundColorSpan(getResources().getColor(R.color.fit_kit_primary_blue)),
                24, // start
                30, // end
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
        );

        mBinding.signInText.setText(spannable);

//        SpannableString string = new SpannableString("Don’t have an account?  Signup");
//        mBinding.signInText.setText(string);
//        mBinding.signInText.setMovementMethod(LinkMovementMethod.getInstance());
//        string.setSpan(clickableSpan, 25, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mBinding.signInText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_from_left);
            }
        });

        mBinding.BtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_from_left);
            }
        });
    }
}