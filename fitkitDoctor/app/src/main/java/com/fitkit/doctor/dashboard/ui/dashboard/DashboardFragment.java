package com.fitkit.doctor.dashboard.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.fitkit.doctor.R;
import com.fitkit.doctor.auth.LoginActivity;
import com.fitkit.doctor.auth.RegistrationActivity;
import com.fitkit.doctor.dashboard.InClinicActivity;
import com.fitkit.doctor.dashboard.VedioCallAppointementsActivity;


public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private LinearLayout VedioCosultation;
    private LinearLayout InClinicConsultation;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
//        final TextView textView = root.findViewById(R.id.text_dashboard);
        VedioCosultation = root.findViewById(R.id.vedio_consultent);
        InClinicConsultation = root.findViewById(R.id.in_clinic_consultation);

        VedioCosultation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), VedioCallAppointementsActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_from_left);

            }
        });

        InClinicConsultation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), InClinicActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_from_left);
            }
        });
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
//                textView.setText(s);
            }
        });
        return root;
    }
}