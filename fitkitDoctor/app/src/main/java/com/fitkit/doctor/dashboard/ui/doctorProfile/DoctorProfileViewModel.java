package com.fitkit.doctor.dashboard.ui.doctorProfile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DoctorProfileViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public DoctorProfileViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is notifications fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}