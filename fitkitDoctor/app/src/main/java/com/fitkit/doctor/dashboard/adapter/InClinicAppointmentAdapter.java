package com.fitkit.doctor.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fitkit.doctor.R;
import com.fitkit.doctor.dashboard.OnclickListener;

import java.util.ArrayList;

public class InClinicAppointmentAdapter extends RecyclerView.Adapter<InClinicAppointmentAdapter.MyInclinicAppointmentViewHolder>{

    private ArrayList<String> mArrayList;
    private Context mContext;
    private OnclickListener mOnclickListener;

    public InClinicAppointmentAdapter(Context context, ArrayList<String> arrayList,OnclickListener onclickListener) {
        mArrayList = arrayList;
        mContext = context;
        mOnclickListener = onclickListener;
    }

    @NonNull
    @Override
    public MyInclinicAppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.in_clinic_consultation_listview, parent, false);
//        RecyclerView.ViewHolder viewHolder = new RecyclerView.ViewHolder(listItem);
        MyInclinicAppointmentViewHolder viewHolder = new MyInclinicAppointmentViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyInclinicAppointmentViewHolder holder, int position) {
        holder.mdoctorName.setText(mArrayList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnclickListener.OnAppointmentClick();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public static class MyInclinicAppointmentViewHolder extends RecyclerView.ViewHolder{

        protected TextView mdoctorName;

        public MyInclinicAppointmentViewHolder(@NonNull View itemView) {
            super(itemView);
            mdoctorName = itemView.findViewById(R.id.doctor_name_txt);
        }
    }
}
