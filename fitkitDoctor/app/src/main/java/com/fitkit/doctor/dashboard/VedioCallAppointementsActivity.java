package com.fitkit.doctor.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.fitkit.doctor.R;
import com.fitkit.doctor.auth.LoginActivity;
import com.fitkit.doctor.auth.RegistrationActivity;
import com.fitkit.doctor.dashboard.adapter.InClinicAppointmentAdapter;
import com.fitkit.doctor.dashboard.adapter.VedioCallApointmentAdapter;
import com.fitkit.doctor.databinding.ActivityVedioCallAppointementsBinding;
import com.fitkit.doctor.profile.PatientDetailsActivity;

import java.util.ArrayList;

public class VedioCallAppointementsActivity extends AppCompatActivity implements OnclickListener {

    private ActivityVedioCallAppointementsBinding mBinding;
    private ArrayList<String> mArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_vedio_call_appointements);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_vedio_call_appointements);

        mBinding.screenTitleTxt.setText("Video Consultations");

        mBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mArrayList.add("Dr. Geeta");
        mArrayList.add("Dr. Sushma");
        mArrayList.add("Dr. Raghu");
        mArrayList.add("Dr. Ramesh");
        VedioCallApointmentAdapter adapter = new VedioCallApointmentAdapter(this,mArrayList, this);
        mBinding.vedioCallAppointments.setHasFixedSize(true);
        mBinding.vedioCallAppointments.setLayoutManager(new LinearLayoutManager(this));
        mBinding.vedioCallAppointments.setAdapter(adapter);
    }

    @Override
    public void OnAppointmentClick() {

        Intent intent = new Intent(VedioCallAppointementsActivity.this, PatientDetailsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_from_left);

    }
}