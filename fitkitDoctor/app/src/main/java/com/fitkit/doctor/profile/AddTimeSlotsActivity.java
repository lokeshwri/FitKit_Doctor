package com.fitkit.doctor.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.fitkit.doctor.R;
import com.fitkit.doctor.databinding.ActivityAddTimeSlotsBinding;
import com.fitkit.doctor.databinding.ActivityPatientDetailsBinding;

public class AddTimeSlotsActivity extends AppCompatActivity {
    private ActivityAddTimeSlotsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_time_slots);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_add_time_slots);

        mBinding.screenTitleTxt.setText("Add time slot");

        mBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}